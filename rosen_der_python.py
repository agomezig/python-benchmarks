# Authors: Travis E. Oliphant (numpy version), Serge Guelton (python version)
# License: BSD
# Source: https://github.com/scipy/scipy/blob/master/scipy/optimize/optimize.py

# Modified by Antonio Gomez at TACC

'''Compute the derivative of the Rosenbrock function

This functions tests the ability of compiler to fuse numpy operators, use
negative indexing and handle memory view instead of making copies when slicing.

see also http://en.wikipedia.org/wiki/Rosenbrock_function
'''

import numpy


def rosen_der_numpy(x):
    xm = x[1:-1]
    xm_m1 = x[:-2]
    xm_p1 = x[2:]
    der = numpy.zeros_like(x)
    der[1:-1] = (+ 200 * (xm - xm_m1 ** 2)
                 - 400 * (xm_p1 - xm ** 2) * xm
                 - 2 * (1 - xm))
    der[0] = -400 * x[0] * (x[1] - x[0] ** 2) - 2 * (1 - x[0])
    der[-1] = 200 * (x[-1] - x[-2] ** 2)
    return der


def rosen_der_python(x):
    n = x.shape[0]
    der = numpy.zeros_like(x)

    for i in range(1, n - 1):
        der[i] = (+ 200 * (x[i] - x[i - 1] ** 2)
                  - 400 * (x[i + 1]
                           - x[i] ** 2) * x[i]
                  - 2 * (1 - x[i]))
    der[0] = -400 * x[0] * (x[1] - x[0] ** 2) - 2 * (1 - x[0])
    der[-1] = 200 * (x[-1] - x[-2] ** 2)
    return der

import numpy as np


N=5000000
rng = np.random.RandomState(42)
x = rng.rand(N)

import time
number_tests = 5 

time_python = np.zeros(number_tests)
time_numpy = np.zeros(number_tests)

for i in xrange(number_tests):
  print "Execution " + str(i+1)
  start = time.time()
  rosen_der_python (x)
  end = time.time()
  time_python[i] = end - start
  print "time python %.4f sec" % (end-start)
  start = time.time()
  rosen_der_numpy (x)
  end = time.time()
  time_numpy[i] = end-start
  print "time numpy %.4f sec" % (end-start)
  print ""

avgtime = np.average( time_python )
print ""
print "     average time python: %.4f sec" % ( avgtime )
avgtime = np.average( time_numpy )
print "     average time numpy: %.4f sec" % ( avgtime )
