# Authors: Jake Vanderplas, Alex Rubinsteyn, Olivier Grisel
# License: MIT
# Modified by Antonio Gomez at TACC

import numpy as np


def pairwise_python_nested_for_loops(data):
    n_samples, n_features = data.shape
    distances = np.empty((n_samples, n_samples), dtype=data.dtype)
    #"omp parallel for private(j, d, k, tmp)"
    for i in range(n_samples):
        for j in range(n_samples):
            d = 0.0
            for k in range(n_features):
                tmp = data[i, k] - data[j, k]
                d += tmp * tmp
            distances[i, j] = np.sqrt(d)
    return distances


def pairwise_python_inner_numpy(data):
    n_samples = data.shape[0]
    result = np.empty((n_samples, n_samples), dtype=data.dtype)
    for i in xrange(n_samples):
        for j in xrange(n_samples):
            result[i, j] = np.sqrt(np.sum((data[i, :] - data[j, :]) ** 2))
    return result


def pairwise_python_broadcast_numpy(data):
    return np.sqrt(((data[:, None, :] - data) ** 2).sum(axis=2))


def pairwise_python_numpy_dot(data):
    X_norm_2 = (data ** 2).sum(axis=1)
    dists = np.sqrt(2 * X_norm_2 - np.dot(data, data.T))
    return dists

shape = (400, 150)
seed = 0

rng = np.random.RandomState(seed)
data = np.asarray(rng.normal(size=shape), dtype=np.double)

import time
number_tests = 5
time_python = np.zeros(number_tests)
time_numpy = np.zeros(number_tests)
time_bcast = np.zeros(number_tests)
time_dot = np.zeros(number_tests)

for i in xrange(number_tests):
  print "Execution " + str(i+1)
  start = time.time()
  pairwise_python_nested_for_loops (data)
  end = time.time()
  time_python[i] = end - start
  print "time python %.4f sec" % (end-start)
  start = time.time()
  pairwise_python_inner_numpy (data)
  end = time.time()
  time_numpy[i] = end-start
  print "time numpy %.4f sec" % (end-start)
  start = time.time()
  pairwise_python_broadcast_numpy (data)
  end = time.time()
  time_bcast[i] = end-start
  print "time bcast %.4f sec" % (end-start)
  start = time.time()
  pairwise_python_numpy_dot (data)
  end = time.time()
  time_dot[i] = end-start
  print "time dot %.4f sec" % (end-start)
  print ""

avgtime = np.average( time_python )
print ""
print "     average time python: %.4f sec" % ( avgtime )
avgtime = np.average( time_numpy )
print "     average time numpy: %.4f sec" % ( avgtime )
avgtime = np.average( time_bcast )
print "     average time bcast: %.4f sec" % ( avgtime )
avgtime = np.average( time_dot )
print "     average time dot: %.4f sec" % ( avgtime )
